package week2.testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class PrivateDashboardCreateandDelete {

	public static void main(String[] args) throws InterruptedException {
		  WebDriverManager.chromedriver().setup();
			
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--disable-notifications");
			ChromeDriver driver = new ChromeDriver(options);
			
			//Login to salesforce application
			driver.get("https://login.salesforce.com/");
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
			
			WebDriverWait wait = new WebDriverWait(driver,30);
			driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
			driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
			driver.findElement(By.id("Login")).click();
			
			
			
			

			String dashboardText="Saravanaa_Workout4";
			String description="Testing";
			String input="Service console";
			
			
			
			//searching for sales application
			wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//div[@class='slds-icon-waffle']"))));	
			driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//button[text()='View All']"))));
			driver.findElement(By.xpath("//button[text()='View All']")).click();
			
			WebElement searchwindow = driver.findElement(By.xpath("//input[contains(@placeholder,'Search apps or items')]"));
			searchwindow.sendKeys(input);
			
			driver.findElement(By.xpath("//mark[text()='Service Console']")).click();
			Thread.sleep(6000);
			
			wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//button[@title='Show Navigation Menu']//lightning-primitive-icon[1]"))));
			driver.findElement(By.xpath("//button[@title='Show Navigation Menu']//lightning-primitive-icon[1]")).click();

			//(//span[text()='Dashboards'])[2]
			
			driver.findElement(By.xpath("//span[contains(@class,'menuLabel') and (text()='Dashboards')]")).click();
			
			Thread.sleep(1000);
			driver.findElement(By.xpath("//div[@title='New Dashboard']")).click();
			
			Thread.sleep(8000);
			//wait.until(ExpectedConditions.presenceOfElementLocated(driver.findElement(By.xpath("//iframe[@title='dashboard']"))));
			driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='dashboard']")));
			Thread.sleep(5000);
			driver.findElement(By.id("dashboardNameInput")).sendKeys(dashboardText);
			Thread.sleep(2000);
			
			
	        driver.findElement(By.id("dashboardDescriptionInput")).sendKeys(description);
			driver.findElement(By.id("submitBtn")).click();
			driver.switchTo().defaultContent();
			Thread.sleep(2000);
			driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='dashboard']")));
	 		Thread.sleep(6000); 
	 		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//button[text()='Done']"))));
	     	driver.findElement(By.xpath("//button[text()='Done']")).click();
	     	Thread.sleep(1000);
	     	String result=driver.findElement(By.xpath("(//span[text()='Dashboard']//following::span)[1]")).getText();
	     	if (result.contains(dashboardText))
			{
				System.out.println("Pass");
				driver.findElement(By.xpath("//button[text()='Subscribe']")).click();
				driver.switchTo().defaultContent();
				Thread.sleep(2000);
				//driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='Edit Subscription']")));
				Thread.sleep(5000);
				driver.findElement(By.xpath("//span[text()='Daily']")).click();
				
				Select se=new Select(driver.findElement(By.id("time")));
				se.selectByIndex(10);
				
				Thread.sleep(1000);
				driver.findElement(By.xpath("//span[text()='Save']")).click();
				
				Thread.sleep(2000);
		     	//String result=driver.findElement(By.xpath("//p[@class='slds-page-header__info']")).getText();
				String result1=driver.findElement(By.xpath("//span[contains(@class,'toastMessage')]")).getText();
		     
				if (result1.contains("ubscription") && result1.contains("tarted"))
				{
					System.out.println("Success");
					Thread.sleep(2000);
					
					//(//span[contains(text(),'Saravanaa_Workout')])[1]
					Thread.sleep(2000);
					//driver.findElement(By.xpath("(//span[contains(text(),'Saravanaa_Workout3')])[1]"));
					//driver.findElement(By.xpath(" (//a[@title='Saravana_Workout'])[1]")).click();
					
					Thread.sleep(2000);
					driver.findElement(By.xpath("//button[@title='Close Saravanaa_Workout4']")).click();
					//(//span[text()='Dashboards'])[1]
					
					Thread.sleep(2000);
					//driver.findElement(By.xpath(" (//a[@title='Saravana_Workout'])[1]")).click();
					driver.findElement(By.xpath("(//span[text()='Dashboards'])[1]")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("(//a[text()='Private Dashboards'])[1]")).click();
					
					Thread.sleep(2000);
					
					Thread.sleep(2000);
					driver.findElement(By.xpath("//input[contains(@placeholder,'Search private dashboards')]")).sendKeys(dashboardText);
					
					
					//driver.findElement(By.xpath("//input[contains(@placeholder,'Search recent dashboards')]")).sendKeys(dashboardText1);
					
					Thread.sleep(5000);
					
					WebElement element=driver.findElement(By.xpath("((//span[text()='Show actions'])//parent::button)[6]"));
					
					JavascriptExecutor je=(JavascriptExecutor)driver;
					je.executeScript("arguments[0].scrollIntoView(true);",element);
					driver.findElement(By.xpath("((//span[text()='Show actions'])//parent::button)[6]")).click();
					
					Thread.sleep(2000);
					
					Thread.sleep(2000);
					driver.findElement(By.xpath("//span[text()='Delete']")).click();
					
					Thread.sleep(2000);
					//(//span[text()='Delete'])[2]
							
					driver.findElement(By.xpath("(//span[text()='Delete'])[2]")).click();
					
					//input[contains(@placeholder,'Search private dashboards')]
					
					Thread.sleep(2000);
			     	//String result=driver.findElement(By.xpath("//p[@class='slds-page-header__info']")).getText();
					String result3=driver.findElement(By.xpath("//span[contains(@class,'toastMessage')]")).getText();
			     	
			     	if (result3.contains("Dashboard") && result3.contains("deleted"))
					{
						System.out.println("Pass");
					}
					else
					{
						System.out.println("Fail");
					}
					
					//driver.switchTo().defaultContent();
					driver.quit();
					
				}
				else
				{
					System.out.println("Failed");
				}
				
			}
			else
			{
				System.out.println("Fail");
			}
			
	}

}
