package week1.testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class createNewDashboard {

	public static void main(String[] args) throws InterruptedException {
        WebDriverManager.chromedriver().setup();
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		
		//Login to salesforce application
		driver.get("https://login.salesforce.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		
		WebDriverWait wait = new WebDriverWait(driver,30);
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
		
		
		
		

		String dashboardText="Salesforce Automation by Saravana";
		String input="Dashboards";
		
		
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//div[@class='slds-icon-waffle']"))));	
		driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//button[text()='View All']"))));
		driver.findElement(By.xpath("//button[text()='View All']")).click();
		WebElement searchwindow = driver.findElement(By.xpath("//input[contains(@placeholder,'Search apps or items')]"));
		searchwindow.sendKeys(input);
		driver.findElement(By.xpath("//mark[text()='Dashboards']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//div[@title='New Dashboard']")).click();
		
		Thread.sleep(5000);
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='dashboard']")));
		driver.findElement(By.id("dashboardNameInput")).sendKeys(dashboardText);
		Thread.sleep(1000);
		driver.findElement(By.id("submitBtn")).click();
        driver.switchTo().defaultContent();
        Thread.sleep(4000); 
        driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='dashboard']")));
		Thread.sleep(4000); 

    	wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//button[text()='Save']"))));
    	driver.findElement(By.xpath("//button[text()='Save']")).click();
    	driver.switchTo().defaultContent();
    	Thread.sleep(5000);
		String result=driver.findElement(By.xpath("//span[@data-aura-class='forceActionsText']")).getText();
		
		if (result.contains("saved") && result.contains("Dashboard"))
		{
			System.out.println("Pass");
		}
		else
		{
			System.out.println("Fail");
		}
		
		driver.close();

		
	}

}
