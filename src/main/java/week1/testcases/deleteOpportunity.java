package week1.testcases;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class deleteOpportunity {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		String expected="Salesforce Automation by Saravana";
			
	WebDriverManager.chromedriver().setup();
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		driver.get("https://login.salesforce.com/");
		driver.manage().window().maximize();
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
		Thread.sleep(8000);
		driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
		//driver.findElement(By.xpath("//button[contains(@class,'slds-icon-waffle')]//div[@class='slds-icon-waffle']")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath("//button[text()='View All']")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//p[text()='Sales']")).click();
		Thread.sleep(5000);
		WebElement element=driver.findElement(By.xpath("//*[text()='Opportunities']"));
		JavascriptExecutor je=(JavascriptExecutor)driver;
		je.executeScript("arguments[0].click();", element);
		//driver.findElement(By.linkText("Opportunities")).click();
		Thread.sleep(6000);
		driver.findElement(By.xpath("//label[text()='Search this list...']/following::input")).sendKeys(expected);
		driver.findElement(By.xpath("//button[@name='refreshButton']")).click();
		Thread.sleep(3000);
		WebElement element1=driver.findElement(By.xpath("//span[contains(@title,'Opportunity Owner')]"));
		JavascriptExecutor je1=(JavascriptExecutor)driver;
		je1.executeScript("arguments[0].click();", element1);
		Thread.sleep(1000);
		WebElement element2=driver.findElement(By.xpath("(//span[contains(text(),'Show Actions')])[1]"));
		JavascriptExecutor je2=(JavascriptExecutor)driver;
		je2.executeScript("arguments[0].click();", element2);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//a[@title='Delete']")).click();
		
		Thread.sleep(1000);
		driver.findElement(By.xpath("//span[text()='Delete']")).click();
		Thread.sleep(2000);
		//String result=driver.findElement(By.xpath("//div[contains(text(),'"+expected+"')]")).getText();
		String result=driver.findElement(By.xpath("//span[contains(@class,'toastMessage')]")).getText();
		System.out.println(result);
//result=result.substring(1,(result.length()-1));
		
		//System.out.println(expected);
		if (result.contains("deleted") && result.contains(expected))
		{
			System.out.println("Pass");
		}
		else
		{
			System.out.println("Fail");
		}
driver.close();
		
	}

}
